# Client Features

Basic feature toggling for browsers.

## Installation

  npm install client-features --save

## Examples

Initialisation:
```js
var ClientFeatures = require('client-features');

window.features = new ClientFeatures({

  landingPage: {
    type:       'ga-experiment',
    experiment: 'XXXXXX',
    variants:   ['original', 'red', 'green']
  },

  actionToolbar: true,
  unitList: true,
  gameGridHexType: 'pointy'
});
```

Usage:
```
if (window.features.unitList) {
  // Do a thing
}

if (window.features.imaginaryFeature) {
  // Will never be triggered!
}

if (window.features.gameGridHexType === 'pointy') {
  // Do a thing
}

switch (window.features.landingPage) {
  case 'red':
    // ????
    break;
  case 'green':
    // ????
    break;
  case 'original':
  default:
    // ????
}


```
## Architectural Goals

- No dependencies
- Simple + easy to use interface
- Ability to extend functionality through plugins
- Small download footprint

## Alternative Libraries

## Advanced

### Google Experiments

### Loading from a `features.json`

## License

[MIT](http://ryanscott.mit-license.org)
