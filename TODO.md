# TODO

- Add testing suite
- Add linting rules
- Add CI
- Add support for popular JS package managers
- Provide a working example in pure js / html
- Add an example of loading the features from a remote JSON file

## Advanced

- Flesh out feature type plugin interface (how to load + run + decide which plugin is used to create a new feature)
- Add back Google Experiment configuration (+ figure out a better solution for the issue with needing to duplicate the experiment id)
- Add an example of setting features from Consul/etcd/other configuration based key value store
- Use a function to determine if a feature is on/off (how does this play with JSON?)
