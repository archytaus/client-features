function FeatureDefinition(name, value) {
  this._name = name
  this._value = value
}

FeatureDefinition.prototype.getValue = function() {
  return this._value;
};

function ClientFeatures(config) {
  this._sourceConfig = config;
  this._features = {};

  var features = this._features;

  function buildFeature(featureName, featureConfig) {
    return new FeatureDefinition(featureName, featureConfig)
  }

  function addFeature(featureName, featureConfig) {
    features[featureName] = buildFeature(featureName, featureConfig)
  }

  Object.keys(config).forEach(function(key) {
    var value = config[key]
    addFeature(key, value)
  });

  var self = this;
  Object.keys(features)
        .forEach(function(featureName) {
          Object.defineProperty(self, {
            get: function() { return this[featureName].getValue() }
          });
        });
}

module.exports["default"] = ClientFeatures;
